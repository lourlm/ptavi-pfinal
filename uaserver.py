
"""
Programa SERVER
"""

import sys
import simplertp
import socketserver
import random
import secrets
from xml.sax import make_parser
from uaclient import log
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    """
        Creamos la clase SmallSMILHandler
    """

    def __init__(self):
        super().__init__()
        self.etiq = {}
        self.dicc = {'account': ['user', 'password'],
                     'uaserver': ['ip', 'puerto'],
                     'rtpaudio': ['puerto'],
                     'regproxy': ['ip', 'puerto'],
                     'log': ['path'],
                     'audio': ['path']}

    def startElement(self, name, attr):
        """
        Cuando accedemos a una etiqueta
        """
        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attr.get(atributos, "")
                self.etiq[name + '_' + atributos] = atributo

    def get_etiq(self):
        """
        Definimos get_etiq
        :return:
        """
        return self.etiq


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Creamos la clase EchoHandler
    """
    diccionarioRTP = {}

    def handle(self):
        """
        Definimos handle
        """
        petition = self.rfile.read().decode('utf-8')
        print("El proxy-registrar nos manda... " + '\r\n' + petition)
        METODO = petition.split()[0]
        log('starting', None, None, None, path)
        log('received', IP_Proxy, PORT_Proxy, petition, path)

        if METODO == 'INVITE':
            LINE = "SIP/2.0 100 Trying" + "\r\n"
            LINE += "SIP/2.0 180 Ring" + "\r\n"
            LINE += "SIP/2.0 200 OK" + "\r\n"
            LINE += "Content-Type: application/sdp\r\n\r\n"
            LINE += "v=0\r\n" + "o=" + username + " "
            LINE += IP_Server + "\r\ns=misesion\r\n"
            LINE += "t=0\r\n" + "m=audio " + PORT_Rtp + " RTP\r\n"

            # Enviamos al proxy
            self.wfile.write(bytes(LINE, 'utf-8'))
            log('send to', IP_Proxy, PORT_Proxy, LINE, path)

        elif METODO == 'ACK':
            BIT = secrets.randbits(1)
            ALEAT = random.randint(1, 15)
            Csrc = []

            ip = petition.split('IP: ')[1]
            PORT = int(petition.split(' PuertoRTP: ')[1])
            self.diccionarioRTP['ip'] = ip
            self.diccionarioRTP['puerto'] = int(PORT)
            ip = self.diccionarioRTP['ip'] = ip
            port = self.diccionarioRTP['puerto'] = int(PORT)
            print(self.diccionarioRTP)

            RTP_cabecera = simplertp.RtpHeader()
            RTP_cabecera.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=ALEAT)

            RTP_cabecera.setCSRC(Csrc)
            audio = simplertp.RtpPayloadMp3(new_audio)
            simplertp.send_rtp_packet(RTP_cabecera, audio, '127.0.0.1', port)

        elif METODO == 'BYE':
            # Enviamos al proxy
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
            request = "SIP/2.0 200 OK"
            log('send to', IP_Proxy, PORT_Proxy, request, path)

        elif METODO != ('INVITE', 'ACK', 'BYE'):
            # Enviamos al proxy
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n")
            request = b"SIP/2.0 405 Method Not Allowed"
            log("error", None, None, request, path)

        else:
            # Enviamos al proxy
            self.wfile.write(b"SIP/2.0 400 Bad Request" + b"\r\n\r\n")
            request = b"SIP/2.0 400 Bad Request"
            log("error", None, None, request, path)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit('Usage: python uaserver.py config')

    CONFIG = sys.argv[1]

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFIG))
    dato = cHandler.get_etiq()

    # Guardamos los datos
    username = dato['account_user']
    password = dato['account_password']
    IP_Server = dato['uaserver_ip']
    PORT_Server = dato['uaserver_puerto']
    PORT_Rtp = dato['rtpaudio_puerto']
    IP_Proxy = dato['regproxy_ip']
    PORT_Proxy = int(dato['regproxy_puerto'])
    path = dato['log_path']
    new_audio = dato['audio_path']

    print("Listening...")
    serv = socketserver.UDPServer((IP_Server, int(PORT_Server)), EchoHandler)

    print("Lanzando servidor...")
    serv.serve_forever()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit('Usage: python uaserver.py config')

    CONFIG = sys.argv[1]

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFIG))
    dato = cHandler.get_etiq()

    # Guardamos los datos
    username = dato['account_user']
    password = dato['account_password']
    IP_Server = dato['uaserver_ip']
    PORT_Server = dato['uaserver_puerto']
    PORT_Rtp = dato['rtpaudio_puerto']
    IP_Proxy = dato['regproxy_ip']
    PORT_Proxy = int(dato['regproxy_puerto'])
    path = dato['log_path']
    new_audio = dato['audio_path']

    print("Listening...")
    serv = socketserver.UDPServer((IP_Server, int(PORT_Server)), EchoHandler)

    print("Lanzando servidor...")
    serv.serve_forever()
