
"""
Programa CLIENTE
"""

import socket
import secrets
import sys
import hashlib
import random
import simplertp
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def log(metodo, IP, PORT, LINE, fichero):
    """
    Definimos la funcion log
    """
    time_now = time.strftime('%Y%m%d%H%M%S')
    file = open(fichero, "a")

    if metodo == 'starting':
        line = (time_now + " Starting...")
        file.write(line + '\r\n')
    elif metodo == 'send to':
        line = time_now + " Sent to " + IP
        line += ':' + str(PORT) + ': ' + LINE
        file.write(line + '\r\n')
    elif metodo == 'received':
        line = time_now + " Received from " + IP
        line += ':' + str(PORT) + ': ' + LINE
        file.write(line + '\r\n')
    elif metodo == 'error':
        line = (time_now + LINE)
        file.write(line + '\r\n')
    elif metodo == 'finishing':
        line = (time_now + " Finishing...")
        file.write(line + '\r\n')

    file.close()


def creamosPassword(passwd, nonce):
    """Devuelve el nonce de respuesta."""
    clave = hashlib.md5()
    clave.update(bytes(passwd, 'utf-8'))
    clave.update(bytes(str(nonce), 'utf-8'))
    return clave.hexdigest()


def hayError(request):
    if int(request.split()[1]) == 400:
        theRequest = "Error: SIP/2.0 400 Bad Request."
        print(theRequest)
        log('error', None, None, theRequest, path)
    elif int(request.split()[1]) == 404:
        theRequest = "Error: SIP/2.0 404 User not found."
        print(theRequest)
        log('error', None, None, theRequest, path)
    elif int(request.split()[1]) == 405:
        theRequest = "Error: SIP/2.0 405 Method Not Allowed."
        print(theRequest)
        log('error', None, None, theRequest, path)


class SmallSMILHandler(ContentHandler):
    """
        Creamos la clase SmallSMILHandler
    """

    def __init__(self):
        super().__init__()
        self.etiq = {}
        self.dicc = {'account': ['user', 'passwd'],
                     'uaserver': ['ip', 'puerto'],
                     'rtpaudio': ['puerto'],
                     'regproxy': ['ip', 'puerto'],
                     'log': ['path'],
                     'audio': ['path']}

    def startElement(self, name, attr):
        """
        Cuando accedemos a una etiqueta
        """
        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attr.get(atributos, "")
                self.etiq[name + '_' + atributos] = atributo

    def get_etiq(self):
        """
        Definimos get_etiq
        :return:
        """
        return self.etiq


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit('Usage: python uaclient.py config method option')

    CONFIG = sys.argv[1]
    METODO = sys.argv[2]
    OPTION = sys.argv[3]

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFIG))
    data = cHandler.get_etiq()

    user = data['account_user']
    password = data['account_passwd']
    IP_Server = data['uaserver_ip']
    PORT_Server = int(data['uaserver_puerto'])
    PORT_Rtp = data['rtpaudio_puerto']
    IP_Proxy = data['regproxy_ip']
    PORT_Proxy = int(data['regproxy_puerto'])
    path = data['log_path']
    new_audio = data['audio_path']

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((IP_Proxy, int(PORT_Proxy)))
        log('starting', None, None, None, path)

        if METODO == 'REGISTER':
            LINE = METODO + " sip:" + user
            LINE += 'IP:' + IP_Proxy
            LINE += ' puerto:' + str(PORT_Server) + " SIP/2.0"
            LINE += " Expires: " + OPTION
            LINE += ' Contrasena usuario: ' + password
            print("Enviando... " + LINE)
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            log('send to', IP_Proxy, PORT_Proxy, LINE, path)
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy: ' + data)

            log('received', IP_Proxy, PORT_Proxy, data, path)
            Request_Server = data.split()
            print(data)
            if Request_Server[1] == '401':
                nonce = creamosPassword(password, Request_Server[8])
                LINE = METODO + ' sip:' + user + ':'
                LINE += str(PORT_Server) + ' SIP/2.0\r\nExpires: '
                LINE += OPTION + '\r\n'
                LINE += 'Authenticate: Digest nonce = ' + nonce + '\r\n\r\n'
                print("REGISTER + autorizacion: " + '\r\n' + LINE)
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            else:
                hayError(data)

        elif METODO == 'INVITE':
            LINE = METODO + " sip: " + user + "\r\n"
            LINE += 'Expires: ' + OPTION + ' SIP/2.0\r\n'
            LINE += "Content-Type: application/sdp\r\n\r\n"
            LINE += "v=0\r\n" + "o=" + user + " " + IP_Server + "\r\n"
            LINE += "s=misesion\r\n" + "t=0\r\n"
            LINE += "m=audio " + PORT_Rtp + " RTP\r\n"
            print('\r\nEnviando al proxy el INVITE... \r\n')
            log('send to', IP_Proxy, PORT_Proxy, LINE, path)

            # Enviamos al proxy
            my_socket.send(bytes(LINE, 'utf-8'))

            data = my_socket.recv(1024).decode('utf-8')
            print('Recibiendo del proxy... \r\n\r\n' + data)
            request = data.split()
            log('received', IP_Proxy, PORT_Proxy, data, path)
            Request_Server = data.split()
            if Request_Server[1] == '100':
                # Enviamos al proxy
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024).decode('utf-8')
                LINE = 'ACK' + ' sip: ' + OPTION + "\r\n" + " SIP/2.0\r\n"
                LINE += 'IP: ' + IP_Server + ' PuertoRTP: ' + PORT_Rtp

                # Enviamos al proxy
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                log('send to', IP_Proxy, PORT_Proxy, LINE, path)

                # Audio
                BIT = secrets.randbits(1)
                ALEAT = random.randint(1, 15)
                csrc = []

                RTP_cabecera = simplertp.RtpHeader()
                RTP_cabecera.set_header(version=2, marker=BIT,
                                        payload_type=14, ssrc=ALEAT)

                port = data.split('m=audio ')[1].split(' RTP\r\n')[0]

                RTP_cabecera.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(new_audio)
                simplertp.send_rtp_packet(RTP_cabecera, audio, '127.0.0.1', int(port))

            else:
                hayError(data)

        elif METODO == 'BYE':
            try:
                LINE = METODO + " sip: " + user
                LINE += "\r\n" + ' SIP/2.0\r\n'

                # Enviamos al proxy
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
                print('Enviando al proxy... ' + LINE)
                log('send to', IP_Proxy, PORT_Proxy, LINE, path)

                data = my_socket.recv(1024).decode('utf-8')
                log('received', IP_Proxy, PORT_Proxy, data, path)

            except ConnectionRefusedError:
                hayError(data)

        else:
            LINE = (METODO + ' sip:' + OPTION + ' SIP/2.0\r\n')

            # Enviamos al proxy
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy-registrar...' + '\r\n' + data)
            print(data)
            hayError(data)

    print("Fin.")
    log('finishing', None, None, None, path)
