
"""
Programa PROXY-REGISTRAR
"""

import socket
import sys
import json
import random
import hashlib
import socketserver
from uaclient import log
from xml.dom import minidom
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    """
    Clase SmallSMILHandler
    """
    def __init__(self):
        super().__init__()
        self.etiq = {}
        self.dicc = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwordpath"],
            "log": ["path"]}

    def startElement(self, name, attr):
        """
        Cuando accedemos a una etiqueta
        """
        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attr.get(atributos, "")
                self.etiq[name + '_' + atributos] = atributo

    def get_etiq(self):
        """
        Definimos get_etiq
        :return:
        """
        return self.etiq


def creamosPassword(passwd, nonce):
    """Devuelve el nonce de respuesta."""
    clave = hashlib.md5()
    clave.update(bytes(str(passwd), 'utf-8'))
    clave.update(bytes(str(nonce), 'utf-8'))
    return clave.hexdigest()


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Clase # SIPRegisterHandler"""
    dicc = {}
    nonce = {}

    def registrarUsuario(self, User, ip, port, TimeExpires):
        """ registrar usuarios en el diccionario """
        self.json2registered()
        self.dicc[User] = {'user': User,
                           'address': ip,
                           'puerto': port,
                           'expires': TimeExpires}
        request = b'SIP/2.0 200 Ok\r\n\r\n'
        self.wfile.write(request)

    def register2json(self):
        """
        Abrimos el fichero y escribimos en el lo del diccionario.
        """
        with open('passwords.json', 'w') as file:
            json.dump(self.dicc, file)

    def json2registered(self):
        """
        Abrimos el fichero y leemos en el para comprobar si existe.
        """
        try:
            with open('passwords.json', 'r') as newfile:
                self.dicc = json.load(newfile)
        except FileNotFoundError:
            pass

    def handle(self):
        """
        Metodo HANDLE done declararemos los metodos
        """

        solicitud = self.rfile.read().decode('utf-8')
        cabecera = 'Via: SIP/2.0/UDP ' + IP_Proxy
        cabecera += ':' + PORT_Proxy + '\r\n\r\n'
        petition = solicitud + cabecera
        print("El cliente nos manda la siguiente peticion: ", petition)

        METODO = solicitud.split()[0]

        if METODO == 'REGISTER':

            if len(petition.split()) == 8:
                self.json2registered()
                self.nonce[METODO] = random.randint(0, 9999999999)
                request = 'SIP/2.0 401 Unauthorized\r\n'
                request += 'WWW Authenticate: Digest nonce = '
                request += str(self.nonce[METODO]) + '\r\n\r\n'
                print('Enviamos al cliente:' + request)
                self.wfile.write(bytes(request, 'utf-8') + b'\r\n')

            elif len(petition.split()) > 8:
                self.json2registered()
                mydoc = minidom.parse(sys.argv[1])
                p = mydoc.getElementsByTagName('database')
                thePassword = p[0].attributes['passwordpath'].value

                Line = hashlib.md5()
                Line.update(bytes(str(self.nonce), 'utf-8'))
                Line.update(bytes(thePassword, 'utf-8'))
                self.register2json()
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

                passwd = solicitud[-5:]
                print("Su contrasena es: ", passwd)
                User = solicitud.split(' sip:')[1].split('IP:')[0]
                ip = solicitud.split('IP:')[1].split(' ')[0]
                port = int(solicitud.split(' puerto:')[1].split(' SIP/2.0')[0])
                TimeExpires = solicitud.split(' Expires: ')[1].split(' Contrasena usuario: ')[0]

                self.registrarUsuario(User, ip, port, TimeExpires)
                myTime1 = solicitud.split(" Expires: ")[1]
                myTime = myTime1.split(' Contrasena usuario: ')[0]
                user = solicitud.split(" sip:")[1].split('IP:')[0]


                # Borramos usuarios
                print('soliiiciciiici borroo', solicitud.split()[4])
                if solicitud.split()[4] == "Expires:":
                    if int(myTime) == 0:
                        del self.dicc[user]
                        print("Borramos usuario.")

                else:
                    print("Guardamos su registro")

        elif METODO == 'INVITE' or 'BYE' or 'ACK':
            self.json2registered()
            print(solicitud)
            USER = solicitud.split(" sip: ")[1].split('\r\n')[0]

            if USER in self.dicc:
                ip = self.dicc[USER]['address']
                port = self.dicc[USER]['puerto']
                
                print('El usuario ', USER, ' aparece registrado con IP: ', ip, ' y puerto: ', port)
                try:
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                        my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        my_socket.connect((ip, int(port)))
                        print('\r\nReenviamos al servidor la solicitud...\r\n')

                        # Enviamos al servidor
                        my_socket.send(bytes(solicitud, 'utf-8') + b'\r\n\r\n')

                        data = my_socket.recv(1024).decode('utf-8')
                        # Enviamos al cliente
                        self.wfile.write(bytes(data, 'utf-8') + b'\r\n\r\n')
                        IP_Client = self.client_address[0]
                        PORT_Client = int(self.client_address[1])

                        log('send to', IP_Client, PORT_Client, data, path)

                except ConnectionRefusedError:
                    print('ERROR')

            elif METODO == 'ACK':
                self.json2registered()
                User = solicitud.split(" sip: ")[1].split(' SIP/2.0\r\n')[0]
                if USER in self.dicc:
                    ip = self.dicc[User]['address']
                    port = self.dicc[User]['puerto']
                    print('El usuario ', User, ' aparece registrado con IP: ', ip, ' y puerto: ', port)
                    try:
                        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                            my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                            my_socket.connect((ip, int(port)))
                            print('\r\nReenviamos al servidor la solicitud...\r\n')

                            # Enviamos al servidor
                            my_socket.send(bytes(solicitud, 'utf-8') + b'\r\n\r\n')

                            data = my_socket.recv(1024).decode('utf-8')
                            # Enviamos al cliente
                            self.wfile.write(bytes(data, 'utf-8') + b'\r\n\r\n')
                            IP_Client = self.client_address[0]
                            PORT_Client = int(self.client_address[1])

                            log('send to', IP_Client, PORT_Client, data, path)

                    except ConnectionRefusedError:
                        print('ERROR')

            else:
                self.json2registered()
                self.wfile.write(b"SIP/2.0 404 User Not Found" + b"\r\n\r\n")
                request = ("SIP/2.0 404 User Not Found" + "\r\n\r\n")
                log('error', None, None, request, path)

        elif METODO != ('INVITE', 'BYE', 'ACK'):
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed...\r\n\r\n")
            request = " Error: SIP/2.0 405 Method Not Allowed..."
            log('error', None, None, request, path)

        else:
            self.wfile.write(b"SIP/2.0 404 User Not Found" + b"\r\n\r\n")
            request = b"SIP/2.0 404 Bad requet"
            log('error', None, None, request, path)

        print(self.dicc, '\r\n\r\n')
        self.register2json()
        log('finishing', None, None, None, path)


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit('Usage: python uaclient.py config method option')

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(sys.argv[1]))
    dato = cHandler.get_etiq()

    user = dato['server_name']
    password = dato['database_passwordpath']
    IP_Proxy = dato['server_ip']
    PORT_Proxy = dato['server_puerto']
    path = dato['log_path']
    database = dato['database_path']

    serv = socketserver.UDPServer((IP_Proxy, int(PORT_Proxy)),
                                  SIPRegisterHandler)

    print("Proxy listening... at port: " + str(PORT_Proxy))

    try:
        serv.serve_forever()

    except FileExistsError:
        print("Servidor finalizado")

    log('finishing', None, None, None, path)
